const convertAraryToString = require('../string5')

const result1 = convertAraryToString(["the", "quick", "brown", "fox"])
console.log(result1)

const result2 = convertAraryToString()
console.log(result2)

const result3 = convertAraryToString([])
console.log(result3)