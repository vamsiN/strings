function getFullName(obj) {

    if (obj === undefined) {
        return ""
    } else {
        return Object.entries(obj).map((each) => {
            return each[1]
        }).join(" ").toUpperCase()
    }


}

module.exports = getFullName