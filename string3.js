
function getMonthName(word) {

    const month = ["January","February","March","April","May","June","July","August","September","October","November","December"];

    if(word === undefined){
        let date = new Date()
        return month[date.getMonth() ]
    }else{
        let monthNum = parseInt(word.split("/")[0])
        if(monthNum > 12){
            return month[parseInt(word.split("/")[1])]
        }else{
            return month[monthNum]
        }
        
    }

    
    
}

module.exports = getMonthName